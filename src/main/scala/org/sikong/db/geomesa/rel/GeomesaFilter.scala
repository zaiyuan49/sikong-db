package org.sikong.db.geomesa.rel

import org.apache.calcite.plan.{RelOptCluster, RelTraitSet}
import org.apache.calcite.rel.RelNode
import org.apache.calcite.rel.`type`.RelDataType
import org.apache.calcite.rel.core.{Filter => CalciteFilter}
import org.apache.calcite.rex.{RexCall, RexInputRef, RexLiteral, RexNode}
import org.apache.calcite.sql.SqlKind._
import org.apache.calcite.sql.validate.SqlValidatorUtil
import org.opengis.filter.expression.Expression
import org.opengis.filter.{Filter => GeoToolsFilter}
import org.sikong.db.core.geomesa.model.GeomesaQuery
import org.sikong.db.geomesa.{GeomesaConstant, ff}

import java.util
import scala.collection.JavaConverters._

/**
 * This class is specially used to convert the original filter conditions into the table
 * filter expressions required by geomesa
 *
 * @param cluster RelOptCluster
 * @param traits  RelTraitSet
 * @param child RelNode
 * @param condition RexNode
 *
 * @author zaiyuan
 * @date 2022/02/27
 */
class GeomesaFilter(cluster: RelOptCluster,
                    traits: RelTraitSet,
                    child: RelNode,
                    rowType: RelDataType,
                    condition: RexNode) extends CalciteFilter(cluster, traits, child, condition) with IGeomesaRelNode {

  val fieldList: util.List[String] = SqlValidatorUtil.uniquify(rowType.getFieldNames, SqlValidatorUtil.EXPR_SUGGESTER, true)

  /**
   * Convert RexNode to GeoTools Filter
   *
   * @param node  RexNode
   * @return  GeoTools Filter
   */
  def convertFilter(node: RexNode): GeoToolsFilter = {
    val call = node.asInstanceOf[RexCall]
    node.getKind match {
      // Unary Logic Converter
      case EQUALS =>                ff.equals(convertExpr(call.operands.get(0)), convertExpr(call.operands.get(1)))
      case NOT_EQUALS =>            ff.notEqual(convertExpr(call.operands.get(0)), convertExpr(call.operands.get(1)))
      case LESS_THAN =>             ff.less(convertExpr(call.operands.get(0)), convertExpr(call.operands.get(1)))
      case LESS_THAN_OR_EQUAL =>    ff.lessOrEqual(convertExpr(call.operands.get(0)), convertExpr(call.operands.get(1)))
      case GREATER_THAN =>          ff.greater(convertExpr(call.operands.get(0)), convertExpr(call.operands.get(1)))
      case GREATER_THAN_OR_EQUAL => ff.greaterOrEqual(convertExpr(call.operands.get(0)), convertExpr(call.operands.get(1)))
      case LIKE =>                  ff.like(convertExpr(call.operands.get(0)), call.operands.get(1).toString)
      case IS_NULL =>               ff.isNull(convertExpr(call.operands.get(0)))
      // Binary Logic Converter
      case AND =>                   ff.and(convertFilter(call.operands.get(0)), convertFilter(call.operands.get(1)))
      case OR =>                    ff.or(convertFilter(call.operands.get(0)), convertFilter(call.operands.get(1)))
      case NOT =>                   ff.not(convertFilter(call.operands.get(0)))
      // GeoTools Spatial Function Converter
      case OTHER_FUNCTION =>
        call.op.toString match {
          case GeomesaConstant.ST_CONTAINS =>     ff.contains(convertExpr(call.operands.get(0)), convertExpr(call.operands.get(1)))
          case GeomesaConstant.ST_CROSSES =>       ff.crosses(convertExpr(call.operands.get(0)), convertExpr(call.operands.get(1)))
          case GeomesaConstant.ST_DISJOINT =>     ff.disjoint(convertExpr(call.operands.get(0)), convertExpr(call.operands.get(1)))
          case GeomesaConstant.ST_EQUALS =>       ff.equals(convertExpr(call.operands.get(0)), convertExpr(call.operands.get(1)))
          case GeomesaConstant.ST_INTERSECTS =>   ff.intersects(convertExpr(call.operands.get(0)), convertExpr(call.operands.get(1)))
          case GeomesaConstant.ST_OVERLAPS =>     ff.overlaps(convertExpr(call.operands.get(0)), convertExpr(call.operands.get(1)))
          case GeomesaConstant.ST_TOUCHES =>      ff.touches(convertExpr(call.operands.get(0)), convertExpr(call.operands.get(1)))
          case GeomesaConstant.ST_WITHIN =>       ff.within(convertExpr(call.operands.get(0)), convertExpr(call.operands.get(1)))
        }
    }
  }

  /**
   * Convert RexNode to GeoTools Expression
   *
   * @param rex RexNode
   * @return Expression
   */
  private def convertExpr(rex: RexNode): Expression = rex match {
    case r: RexCall =>
      r.getKind match {
        case OTHER_FUNCTION =>      ff.function(r.op.toString, r.operands.asScala.map(convertExpr).toArray)
        case PLUS =>                ff.add(convertExpr(r.operands.get(0)), convertExpr(r.operands.get(1)))
        case MINUS =>               ff.subtract(convertExpr(r.operands.get(0)), convertExpr(r.operands.get(1)))
        case TIMES =>               ff.multiply(convertExpr(r.operands.get(0)), convertExpr(r.operands.get(1)))
        case DIVIDE =>              ff.divide(convertExpr(r.operands.get(0)), convertExpr(r.operands.get(1)))
        // TODO
        // Cast is a basic operation of SQL, which contains many situations. However, OpenGIS itself does not support
        // cast, so some complex operations have not been implemented. Now only relatively simple logic (simple fields
        // and values) are implemented. The nested operations contained in it will be implemented later.
        case CAST =>                convertExpr(r.operands.get(0))
        case _ =>                   throw new Exception("Unsupported operator")
      }
    case r: RexInputRef =>          ff.property(fieldList.get(r.getIndex))
    case r: RexLiteral =>           ff.literal(RexLiteral.value(r))
  }

  override def copy(traitSet: RelTraitSet, input: RelNode, condition: RexNode): CalciteFilter =
    new GeomesaFilter(cluster, traitSet, input, rowType, condition)

  override def wrap(query: GeomesaQuery): Unit = query.setFilter(convertFilter(condition))
}
